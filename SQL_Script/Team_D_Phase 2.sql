CREATE USER dbuser WITH PASSWORD '123456';
CREATE DATABASE 425PRJ OWNER dbuser;
GRANT ALL PRIVILEGES ON DATABASE 425PRJ TO dbuser;


    CREATE TABLE Address (
        id NUMBER(19,0) NOT NULL PRIMARY KEY ,
        Street VARCHAR2(50) NOT NULL,
        Street_number VARCHAR2(50) NOT NULL,
        Street_name VARCHAR2(50) NOT NULL,
        apt_number VARCHAR2(50),
        City VARCHAR2(50) NOT NULL,
        State VARCHAR2(50) NOT NULL,
        Zip VARCHAR2(20) NOT NULL,
    ) ;

    CREATE TABLE CreditCard (
        id NUMBER(19,0) NOT NULL PRIMARY KEY ,
        Card_Holdname  VARCHAR2(50) NOT NULL,
        Card_number  NUMBER(20) NOT NULL,
        Validate VARCHAR2(20) NOT NULL,
        CVV NUMBER(10) NOT NULL,
        Payment_Address_ID NUMBER(19,0),
        constraint PAYMENT_ADDRESS_ID_FK foreign key (Payment_Address_ID) references Address(id) 
    ) ;

    CREATE TABLE Customers (
        id NUMBER(19,0) NOT NULL PRIMARY KEY ,
        Name VARCHAR2(50) NOT NULL,
        Address_ID NUMBER(19,0) NOT NULL,
        Address_Type VARCHAR2(20) NOT NULL,
        CreditCard_ID NUMBER(19,0) NOT NULL,
        Account_Balance NUMBER(19,2),
        constraint ADDRESS_ID_FK foreign key (Address_ID) references Address(id),
        constraint CREDITCARD_ID_FK foreign key (CreditCard_ID) references CreditCard(id)
    ) ;

    CREATE TABLE Order (
        id NUMBER(19,0) NOT NULL PRIMARY KEY ,
        Customer_ID NUMBER(19,0) NOT NULL,
        Staff_ID NUMBER(19,0),
        Payment_ID NUMBER(19,0),
        Create_Date VARCHAR2(20),
        Update_Date VARCHAR2(20),
        constraint CUSTOMER_ID_FK foreign key (Customer_ID) references Customers(id),
        constraint STAFF_ID_FK foreign key (Staff_ID) references StaffMember(id),
        constraint PAYMENT_ID_FK foreign key (Payment_ID) references Payment(id)
    ) ;

    CREATE TABLE StaffMember (
        id NUMBER(19,0) NOT NULL PRIMARY KEY ,
        Name VARCHAR2(50) NOT NULL,
        Address_ID NUMBER(19,0) NOT NULL, 
        Salary NUMBER(19,2),
        Job_Title VARCHAR2(50) NOT NULL,
        Password VARCHAR2(50) NOT NULL,
        constraint ADDRESS_ID_FK foreign key (Address_ID) references Address(id)
    ) ;

    CREATE TABLE Payment (
        id NUMBER(19,0) NOT NULL PRIMARY KEY ,
        Address_ID NUMBER(19,0),
        Name VARCHAR2(50) NOT NULL,
        Card_Name VARCHAR2(50) NOT NULL,
        constraint ADDRESS_ID_FK foreign key (Address_ID) references Address(id)
    ) ;
    
    CREATE TABLE OrderDetail (
        Order_ID NUMBER(19,0) NOT NULL PRIMARY KEY ,
        Pro_ID NUMBER(19,0) NOT NULL,
        Value NUMBER(19,2)
        constraint ORDER_ID_FK foreign key (Order_ID) references Order(id),
        constraint PRODUCT_STOCK_FK foreign key (Pro_ID) references Product(id)
    ) ;
    
------------------------------------------------------------------------------
    CREATE TABLE Warehouse  (
        id NUMBER(19,0) NOT NULL PRIMARY KEY ,
        verison NUMBER(19,0) ,
        Name VARCHAR2(50) NOT NULL,
        Address VARCHAR2(100) NOT NULL,
        Capacity NUMBER(19,0)
    ) ;
    
    CREATE TABLE Stock  (
        Pro_ID NUMBER(19,0)  ,
        Warehouse_ID NUMBER(19,0)  ,
        Quantity NUMBER(19,0)  ,
        constraint WHEREHOUSE_STOCK_FK foreign key (Warehouse_ID) references Warehouse(id),
        constraint PRODUCT_STOCK_FK foreign key (Pro_ID) references Product(id)
    ) ;
    
    CREATE TABLE Suppliers  (
        ID NUMBER(19,0)  NOT NULL PRIMARY KEY ,
        Name VARCHAR2(50) NOT NULL,
        Address_ID NUMBER(19,0)  ,
        constraint SUPPLIERS_ADDRESS_FK foreign key (Address_ID) references Address(id)
    ) ;
    
    CREATE TABLE SupplierPro  (
        Supplier_ID NUMBER(19,0)  ,
        Pro_ID NUMBER(19,0)  ,
        Price NUMBER(19,0)  ,
        constraint SUPPLIERSPRO_SUPPLIER_FK foreign key (Supplier_ID) references Suppliers(id),
        constraint SUPPLIERSPRO_PRODUCT_FK foreign key (Pro_ID) references Product(id)
    ) ;
    
    CREATE TABLE Product  (
    id NUMBER(19,0)  NOT NULL PRIMARY KEY ,
    name VARCHAR2(50) NOT NULL,
    valume VARCHAR2(10) ,
    type VARCHAR2(10) ,
    add_info_id NUMBER(19,0) ,
    category_id NUMBER(19,0) ,
    quantity NUMBER(19,0)  ,
    image  VARCHAR2(50),
    price NUMBER(19,2) ,
        constraint Product_CATEGORY_FK foreign key (category_id) references  ProductCategory(id),
        constraint Product_AD_INFO_FK foreign key (add_info_id) references  AdditionalInfo(id)
    ) ;
    
    CREATE TABLE AdditionalInfo  (
    Pro_ID NUMBER(19,0) ,
    Type VARCHAR2(50) NOT NULL,
    Info_Name VARCHAR2(50) NOT NULL,
    Info_Value VARCHAR2(50) NOT NULL,
        constraint ADDITIONAL_INFO_PRODUCT_FK foreign key (Pro_ID) references Product(id)
    ) ;
    
    CREATE TABLE ProductCategory  (
    id NUMBER(19,0)  NOT NULL PRIMARY KEY ,
    name VARCHAR2(50) NOT NULL,
    unit VARCHAR2(10) ,
    ) ;
    
    CREATE SEQUENCE ADDRESS_ID_SEQ START WITH 0 MINVALUE 0 MAXVALUE 9223372036854775807 NOCYCLE;
    CREATE SEQUENCE CREDITCARD_ID_SEQ START WITH 0 MINVALUE 0 MAXVALUE 9223372036854775807 NOCYCLE;
    CREATE SEQUENCE CUSTOMERS_ID_SEQ START WITH 0 MINVALUE 0 MAXVALUE 9223372036854775807 NOCYCLE;
    CREATE SEQUENCE ORDER_ID_SEQ START WITH 0 MINVALUE 0 MAXVALUE 9223372036854775807 NOCYCLE;
    CREATE SEQUENCE STAFFMEMEBER_ID_SEQ START WITH 0 MINVALUE 0 MAXVALUE 9223372036854775807 NOCYCLE;
    CREATE SEQUENCE PAYMENT_ID_SEQ START WITH 0 MINVALUE 0 MAXVALUE 9223372036854775807 NOCYCLE;
    
    CREATE SEQUENCE WAREHOUSE_ID_SEQ START WITH 0 MINVALUE 0 MAXVALUE 9223372036854775807 NOCYCLE;
    CREATE SEQUENCE SUPPLIER_ID_SEQ START WITH 0 MINVALUE 0 MAXVALUE 9223372036854775807 NOCYCLE;
    CREATE SEQUENCE PRODUCT_ID_SEQ START WITH 0 MINVALUE 0 MAXVALUE 9223372036854775807 NOCYCLE;
    CREATE SEQUENCE CATEGORY_ID_SEQ START WITH 0 MINVALUE 0 MAXVALUE 9223372036854775807 NOCYCLE;
   
    
