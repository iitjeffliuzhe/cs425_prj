package com.iit.controller;

import com.iit.handler.AbstractCRUDHandler;
import com.iit.service.AddressService;
import com.iit.service.dto.AddressDTO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author william
 */
@RestController
@RequestMapping(value = "/address")
public class AddressController extends AbstractCRUDHandler<Long, AddressDTO, AddressService> {

}