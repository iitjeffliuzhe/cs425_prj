package com.iit.controller;

import com.iit.handler.AbstractCRUDHandler;
import com.iit.service.CustomerService;
import com.iit.service.dto.CustomerDTO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author william
 */
@RestController
@RequestMapping(value = "/customer")
public class CustomerController extends AbstractCRUDHandler<Long, CustomerDTO, CustomerService> {

}