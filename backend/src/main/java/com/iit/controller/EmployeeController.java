package com.iit.controller;

import com.iit.handler.AbstractCRUDHandler;
import com.iit.service.EmployeeService;
import com.iit.service.dto.EmployeeDTO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author william
 */
@RestController
@RequestMapping(value = "/employee")
public class EmployeeController extends AbstractCRUDHandler<Long, EmployeeDTO, EmployeeService> {

}