package com.iit.controller;

import com.iit.handler.AbstractCRUDHandler;
import com.iit.handler.ControllerTemplate;
import com.iit.handler.GenericResponse;
import com.iit.service.InventoryService;
import com.iit.service.dto.InventoryDTO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author william
 */
@RestController
@RequestMapping(value = "/inventory")
public class InventoryController extends AbstractCRUDHandler<Long, InventoryDTO, InventoryService> {

    @PostMapping(value = "/inbound")
    public GenericResponse<InventoryDTO> inbound(@RequestBody InventoryDTO request) throws Exception {

        return ControllerTemplate.call((GenericResponse<InventoryDTO> response) -> {

            response.setData(getService().inbound(request.getModelId(), request.getQuantity()));
            response.setResult(true);
        });
    }


    @PostMapping(value = "/outbound")
    public GenericResponse<InventoryDTO> outbound(@RequestBody InventoryDTO request) throws Exception {

        return ControllerTemplate.call((GenericResponse<InventoryDTO> response) -> {

            response.setData(getService().outbound(request.getModelId(), request.getQuantity()));
            response.setResult(true);
        });
    }
}