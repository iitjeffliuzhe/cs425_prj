package com.iit.controller;

import com.iit.handler.AbstractCRUDHandler;
import com.iit.service.ModelService;
import com.iit.service.dto.ModelDTO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author william
 */
@RestController
@RequestMapping(value = "/model")
public class ModelController extends AbstractCRUDHandler<Long, ModelDTO, ModelService> {

}