package com.iit.controller;

import com.iit.handler.AbstractCRUDHandler;
import com.iit.service.OrderService;
import com.iit.service.dto.OrderDTO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author william
 */
@RestController
@RequestMapping(value = "/order")
public class OrderController extends AbstractCRUDHandler<Long, OrderDTO, OrderService> {


}