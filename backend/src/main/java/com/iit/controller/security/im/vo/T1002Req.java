package com.iit.controller.security.im.vo;

/**
 * .
 * 根据类别代码查询所有大类
 */
public class T1002Req {
    private String typeCode;

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }
}
