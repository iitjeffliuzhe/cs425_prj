/*
 * Copyright (c)  2016, dabing.io
 * All rights reserved.
 * PermissionVo.java 2016-02-26 19:13
 */

package com.iit.controller.security.vo;

/**
 * 描述: 角色授权资源对象
 *

 * @since 1.0
 */
public class PermissionVo {

    private String resourceCode;

    public String getResourceCode() {
        return resourceCode;
    }

    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }
}