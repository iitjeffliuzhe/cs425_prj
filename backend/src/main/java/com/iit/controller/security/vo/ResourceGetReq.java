/*
 * Copyright (c)  2016, dabing.io
 * All rights reserved.
 * ResourceGetReq.java 2016-02-24 16:37
 */

package com.iit.controller.security.vo;

/**
 * 描述: Resource 获取资源明细请求对象
 *

 * @since 1.0
 */
public class ResourceGetReq {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}