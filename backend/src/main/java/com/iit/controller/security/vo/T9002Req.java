/*
 * Copyright (c)  2015, dabing.io
 * All rights reserved. 
 * T9002Req.java 15/10/19 下午3:58
 */
package com.iit.controller.security.vo;

/**
 * 描述：管理人员 明细信息获取请求对象
 *
 *
 * @since 1.0
 */
public class T9002Req {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}