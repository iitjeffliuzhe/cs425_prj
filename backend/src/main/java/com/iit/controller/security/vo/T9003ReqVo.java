/*
 * Copyright (c)  2015, dabing.io
 * All rights reserved. 
 * T9003ReqVo.java 15/10/19 下午4:00
 */
package com.iit.controller.security.vo;

/**
 * 描述：管理人员 删除
 *
 *
 * @since 1.0
 */
public class T9003ReqVo {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}