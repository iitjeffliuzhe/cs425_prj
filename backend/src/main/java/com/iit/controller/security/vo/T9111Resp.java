/*
 * Copyright (c)  2015, dabing.io
 * All rights reserved. 
 * T9111Resp.java  下午6:25
 */
package com.iit.controller.security.vo;

/**
 * 描述：添加角色用户响应
 *
 * @since 1.0
 */
public class T9111Resp {

    private String employeeNo;
    private String userName;

    public String getEmployeeNo() {
        return employeeNo;
    }

    public void setEmployeeNo(String employeeNo) {
        this.employeeNo = employeeNo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
