/*
 * Copyright (c)  2015, dabing.io
 * All rights reserved. 
 * T9123ReqVo.java  22:33
 */
package com.iit.controller.security.vo;

/**
 * 描述：删除授权资源
 *
  on .
 * @since 1.0
 */
public class T9121ReqVo {
    private Long id;
    private String resourceCode;
    private String resourceName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getResourceCode() {
        return resourceCode;
    }

    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }
}