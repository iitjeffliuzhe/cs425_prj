/*
 * Copyright (c)  2015, dabing.io
 * All rights reserved.
 * Resource.java 2015-11-01 12:06:33
 */
package com.iit.controller.security.vo;

/**
 * 描述：根据ID获取对象请求
 *
 *
 * @since 1.0
*/
public class T9202Req {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}