package com.iit.controller.security.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * Title:
 * </p >
 * <p>
 * Description:
 * </p >
 * <p>
 * Copyright: Copyright (c) 2019
 * All rights reserved. 2019-06-21.
 * </p >
 *
 * @author MaoJiaWei
 * @version 1.0
 */
@Getter
@Setter
public class ViewPermissionDO {
    /**
     * 资源id
     */
    private Object resourceid;
    /**
     * 资源代码
     */
    private Object resourcecode;
    /**
     * 资源名称
     */
    private Object resourcename;
    /**
     * 上级代码
     */
    private Object parentcode;
    /**
     * 是否为菜单
     */
    private Object ismenu;
    /**
     * 是否含有上级资源代码
     */
    private Object presourcecode;
}
