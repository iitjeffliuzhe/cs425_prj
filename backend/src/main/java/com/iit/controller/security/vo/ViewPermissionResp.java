/*
 * Copyright (c)  2016, dabing.io
 * All rights reserved.
 * ViewPermissionResp.java 2016-02-26 19:05
 */

package com.iit.controller.security.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * 描述: 已有资源权限预览响应对象
 *
 * @since 1.0
 */
@Getter
@Setter
public class ViewPermissionResp {
    //大写的名称用于接收sql查询返回,小写名称用于页面渲染使用
    /**
     * 资源编号
     */
    private Object resourceId;
    /**
     * 资源代码
     */
    private Object resourceCode;
    /**
     * 资源名称
     */
    private Object resourceName;
    /**
     * 上级资源代码
     */
    private Object parentCode;
    /**
     * 是否菜单
     */
    private Object isMenu;
    /**
     * 是否为权限
     */
    private Object permissionBoolean;


}