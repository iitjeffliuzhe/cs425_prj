package com.iit.domain.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Persistable;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * @author william
 */
@Getter
@Setter
@MappedSuperclass
public abstract class AbstractPersistableEntity<ID extends Serializable> implements Persistable<ID> {

    private static final long serialVersionUID = -1L;

    public abstract void setId(ID id);

    @Transient
    public boolean isNew() {
        return null == this.getId();
    }
}
