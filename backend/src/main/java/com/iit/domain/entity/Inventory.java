package com.iit.domain.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "inventory")
@Getter
@Setter
@Entity
public class Inventory extends AbstractPersistableEntity<Long> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(name = "lent_time", nullable = false)
    private Date lentTime;

    @Column(name = "category_type")
    private String categoryType;

    @Column(name = "quantity")
    private BigDecimal quantity;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinTable(name = "inventory_model",
            joinColumns = {@JoinColumn(name = "inventory_id", insertable = false,
                    updatable = false, referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "model_id", insertable = false,
                    updatable = false, referencedColumnName = "id")}
    )
    @EqualsAndHashCode.Exclude
    private Model model;
}