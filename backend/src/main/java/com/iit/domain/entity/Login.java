package com.iit.domain.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

@Table(name = "login")
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Login extends AbstractPersistableEntity<Long> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(name = "login_time", nullable = false)
    private Timestamp loginTime;

    @Column(name = "ip")
    private String ip;

    @Column(name = "logout_time", nullable = false)
    private Timestamp logoutTime;

    @Column(name = "employee_id")
    private Integer employeeId;

    
}