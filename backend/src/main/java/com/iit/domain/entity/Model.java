package com.iit.domain.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Table(name = "model")
@Getter
@Setter
@Entity
public class Model extends AbstractPersistableEntity<Long> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "model_no")
    private String modelNo;

    @Column(name = "sale_price")
    private BigDecimal salePrice;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "inventory_model",
            joinColumns = {@JoinColumn(name = "model_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "inventory_id", referencedColumnName = "id")}
    )
    @EqualsAndHashCode.Exclude
    private Inventory inventory;
}