package com.iit.domain.repository;

import com.iit.domain.entity.AvaliableInventory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AvaliableInventoryRepository extends JpaRepository<AvaliableInventory, Long>, JpaSpecificationExecutor<AvaliableInventory> {

}