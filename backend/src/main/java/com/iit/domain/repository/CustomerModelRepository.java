package com.iit.domain.repository;

import com.iit.domain.entity.CustomerModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CustomerModelRepository extends JpaRepository<CustomerModel, Long>, JpaSpecificationExecutor<CustomerModel> {

}