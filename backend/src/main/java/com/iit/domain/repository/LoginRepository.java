package com.iit.domain.repository;

import com.iit.domain.entity.Login;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface LoginRepository extends JpaRepository<Login, Long>, JpaSpecificationExecutor<Login> {

}