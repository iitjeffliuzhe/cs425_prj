package com.iit.domain.repository;

import com.iit.domain.entity.Premission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PremissionRepository extends JpaRepository<Premission, Long>, JpaSpecificationExecutor<Premission> {

}