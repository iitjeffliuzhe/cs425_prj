package com.iit.handler;

import com.iit.service.base.CRUDService;
import com.iit.service.base.CRUDServiceAware;
import com.iit.service.base.PageableService;
import com.iit.util.ReflectionUtils;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

/**
 * @author william
 */
@Slf4j
public abstract class AbstractCRUDHandler<ID, T, S extends CRUDService<ID, T>> extends GenericHandler
        implements CRUDServiceAware<S>, BeanFactoryAware {

    private Class<S> defaultServiceClass;

    @Setter
    private BeanFactory beanFactory;

    @SuppressWarnings("unchecked")
    protected AbstractCRUDHandler() {
        defaultServiceClass = ReflectionUtils.getClassGenericType(getClass(), 2);
    }

    @Override
    public S getService() {
        return beanFactory.getBean("default" + defaultServiceClass.getSimpleName(), defaultServiceClass);
    }

    @PostMapping
    public GenericResponse<T> create(@RequestBody @Valid T request, BindingResult bindingResult) throws Exception {

        return ControllerTemplate.call(bindingResult, (GenericResponse<T> response) -> {

            response.setData(getService().create(request));
            response.setResult(true);
        });
    }

    @GetMapping(value = "/{id}")
    public GenericResponse<T> retrieve(@PathVariable("id") Long id) throws Exception {

        return ControllerTemplate.call((GenericResponse<T> response) -> {

            response.setData(getService().retrieve((ID) id));
            response.setResult(true);
        });
    }

    @PutMapping
    public GenericResponse<Void> update(@RequestBody @Valid T request, BindingResult bindingResult) throws
            Exception {

        return ControllerTemplate.call(bindingResult, (GenericResponse<Void> response) -> {
            getService().update(request);
            response.setResult(true);
        });
    }

    @DeleteMapping(value = "/{id}")
    public GenericResponse<Void> delete(@PathVariable("id") Long id) throws Exception {

        return ControllerTemplate.call((GenericResponse<Void> response) -> {

            response.setResult(getService().delete((ID) id));
        });
    }

    @DeleteMapping
    public GenericResponse<Void> delete() throws Exception {

        return ControllerTemplate.call((GenericResponse<Void> response) -> {

            getService().deleteAll();
            response.setResult(true);
        });
    }

    @GetMapping(value = "/listAll")
    public GenericResponse<List<T>> listAll() throws Exception {

        return ControllerTemplate.call((GenericResponse<List<T>> response) -> {

            List allData = ((PageableService) getService()).listAllActive();
            extractResponse(response, new PageImpl(allData));
        });
    }

    @GetMapping(value = "/listAll/{num}")
    public GenericResponse<List<T>> listAll(@PathVariable(value = "num") Integer num) throws Exception {

        return internalListAll(num, null);
    }

    @GetMapping(value = "/listAll/{num}/{page_size}")
    public GenericResponse<List<T>> listAll(@PathVariable(value = "num") Integer num,
                                            @PathVariable(value = "page_size") Integer pageSize) throws Exception {

        return internalListAll(num, pageSize);
    }

    @GetMapping(value = "/list")
    public GenericResponse<List<T>> list() throws Exception {

        return internalList(null, null);
    }

    @GetMapping(value = "/list/{num}")
    public GenericResponse<List<T>> list(@PathVariable(value = "num") Integer num) throws Exception {

        return internalList(num, null);
    }

    @GetMapping(value = "/list/{num}/{page_size}")
    public GenericResponse<List<T>> list(@PathVariable(value = "num") Integer num,
                                         @PathVariable(value = "page_size") Integer pageSize) throws Exception {
        return internalList(num, pageSize);
    }

    @PostMapping(value = "/search")
    public GenericResponse<List<T>> query(@RequestBody @Valid T request, BindingResult bindingResult) throws
            Exception {

        return internalQuery(null, null, request);
    }

    @PostMapping(value = "/search/{num}")
    public GenericResponse<List<T>> query(@PathVariable(value = "num") Integer num,
                                          @RequestBody @Valid T request, BindingResult bindingResult) throws Exception {
        return internalQuery(num, null, request);
    }

    @PostMapping(value = "/search/{num}/{page_size}")
    public GenericResponse<List<T>> query(@PathVariable(value = "num") Integer num,
                                          @PathVariable(value = "page_size") Integer pageSize,
                                          @RequestBody @Valid T request, BindingResult bindingResult) throws Exception {
        return internalQuery(num, pageSize, request);
    }


    private GenericResponse<List<T>> internalListAll(Integer pageNumber, Integer pageSize) throws Exception {
        return ControllerTemplate.call((GenericResponse<List<T>> response) -> {

            Page<T> data = ((PageableService) getService()).listAll(getPageRequest(pageNumber, pageSize));

            extractResponse(response, data);
        });
    }

    private GenericResponse<List<T>> internalList(Integer pageNumber, Integer pageSize) throws Exception {
        return ControllerTemplate.call((GenericResponse<List<T>> response) -> {

            Page<T> data = ((PageableService) getService()).listActive(getPageRequest(pageNumber, pageSize));

            extractResponse(response, data);
        });
    }

    private GenericResponse<List<T>> internalQuery(Integer pageNumber, Integer pageSize, T request) throws Exception {
        return ControllerTemplate.call((GenericResponse<List<T>> response) -> {

            Page<T> data = ((PageableService) getService()).list(request, getPageRequest(pageNumber, pageSize));

            extractResponse(response, data);
        });
    }

    int defaultPageSize() {
        return 10;
    }

    int defaultPageNumber() {
        return 0;
    }

    protected Pageable getPageRequest(Integer pageNumber, Integer pageSize) {

        return getPageRequest(pageNumber, pageSize, null);
    }

    protected Pageable getPageRequest(Integer pageNumber, Integer pageSize, Sort sort) {

        PageRequest pageRequest;

        if (null == sort) {
            sort = getSort();
        }

        if (null != sort) {
            pageRequest = PageRequest.of(setAndGetPageNumber(pageNumber), setAndGetPageSize(pageSize), sort);
        } else {
            pageRequest = PageRequest.of(setAndGetPageNumber(pageNumber), setAndGetPageSize(pageSize));
        }
        return pageRequest;
    }


    protected Sort getSort() {
        return null;
    }

    protected int setAndGetPageSize(Integer pageSize) {
        if (null == pageSize) {
            return defaultPageSize();
        } else {
            return pageSize;
        }
    }

    protected int setAndGetPageNumber(Integer pageNumber) {
        if (null == pageNumber) {
            return defaultPageNumber();
        } else {
            if (pageNumber > 0) {
                return pageNumber - 1;
            }
            return pageNumber;
        }
    }
}
