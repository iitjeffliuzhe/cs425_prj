package com.iit.handler;

import java.text.ParseException;

/**
 * @author william
 */
@FunctionalInterface
public interface ControllerCallback<RS> {

    void execute(GenericResponse<RS> response) throws ParseException;
}
