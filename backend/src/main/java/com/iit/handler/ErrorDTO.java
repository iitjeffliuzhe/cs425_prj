package com.iit.handler;

import lombok.Data;

/**
 * @author william
 */
@Data
public class ErrorDTO {

    private String code;

    private String message;
}
