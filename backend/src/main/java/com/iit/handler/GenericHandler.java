package com.iit.handler;

import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author william
 */
public abstract class GenericHandler {

    protected <T> void extractResponse(GenericResponse<List<T>> response, Page<T> data) {

        response.setData(data.getContent());

        PageDTO pageDTO = new PageDTO();

        pageDTO.setNumber(data.getNumber() + 1);
        pageDTO.setNumberOfElements(data.getNumberOfElements());
        pageDTO.setSize(data.getSize());
        pageDTO.setTotalPages(data.getTotalPages());
        pageDTO.setTotalElements(data.getTotalElements());

        response.setPage(pageDTO);
        response.setResult(true);
    }
}
