package com.iit.handler;

import lombok.Data;

/**
 * @author william
 */
@Data
public class GenericResponse<T> {

    private boolean result;

    private ErrorDTO error;

    private T data;

    private PageDTO page;
}
