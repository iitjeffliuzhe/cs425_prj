package com.iit.service;

import com.iit.service.base.CRUDService;
import com.iit.service.dto.AddressDTO;

/**
 * @author william
 */
public interface AddressService extends CRUDService<Long, AddressDTO> {
}
