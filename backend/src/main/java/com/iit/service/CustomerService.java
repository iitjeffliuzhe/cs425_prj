package com.iit.service;

import com.iit.service.base.CRUDService;
import com.iit.service.dto.CustomerDTO;

/**
 * @author william
 */
public interface CustomerService extends CRUDService<Long, CustomerDTO> {
}
