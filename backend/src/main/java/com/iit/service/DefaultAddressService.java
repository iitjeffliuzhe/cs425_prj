package com.iit.service;

import com.iit.domain.entity.Address;
import com.iit.domain.repository.AddressRepository;
import com.iit.service.base.AbstractStatelessJPAService;
import com.iit.service.dto.AddressDTO;
import org.springframework.stereotype.Service;

/**
 * @author william
 */
@Service
public class DefaultAddressService extends AbstractStatelessJPAService<Long, AddressDTO, Address, AddressRepository> implements AddressService {

    @Override
    public AddressDTO toDTO(Address entity) {
        return null;
    }

    @Override
    public Address toEntity(AddressDTO dto) {
        return null;
    }

    @Override
    public void updateEntity(AddressDTO dto, Address entity) {

    }
}
