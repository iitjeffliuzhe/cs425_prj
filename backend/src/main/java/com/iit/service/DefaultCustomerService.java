package com.iit.service;

import com.iit.domain.entity.Customer;
import com.iit.domain.repository.CustomerRepository;
import com.iit.service.base.AbstractStatelessJPAService;
import com.iit.service.dto.CustomerDTO;
import org.springframework.stereotype.Service;

/**
 * @author william
 */
@Service
public class DefaultCustomerService extends AbstractStatelessJPAService<Long, CustomerDTO, Customer, CustomerRepository> implements CustomerService {

    @Override
    public void updateEntity(CustomerDTO dto, Customer entity) {

    }
}
