package com.iit.service;

import com.iit.domain.entity.Employee;
import com.iit.domain.repository.EmployeeRepository;
import com.iit.service.base.AbstractStatelessJPAService;
import com.iit.service.dto.EmployeeDTO;
import com.iit.util.CryptoUtils;
import org.springframework.stereotype.Service;

/**
 * @author william
 */
@Service
public class DefaultEmployeeService extends AbstractStatelessJPAService<Long, EmployeeDTO, Employee, EmployeeRepository> implements EmployeeService {

    @Override
    public void updateEntity(EmployeeDTO dto, Employee entity) {

    }

    @Override
    protected void createHandler(EmployeeDTO dto, Employee entity) {
        entity.setPassword(CryptoUtils.md5Hash(dto.getPassword()));
    }
}
