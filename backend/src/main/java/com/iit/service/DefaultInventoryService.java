package com.iit.service;

import com.iit.domain.entity.Inventory;
import com.iit.domain.entity.Model;
import com.iit.domain.repository.InventoryRepository;
import com.iit.service.base.AbstractStatelessJPAService;
import com.iit.service.dto.InventoryDTO;
import com.iit.service.dto.ModelDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * @author william
 */
@Service
public class DefaultInventoryService extends AbstractStatelessJPAService<Long, InventoryDTO, Inventory, InventoryRepository> implements InventoryService {

    @Override
    public InventoryDTO toDTO(Inventory entity) {
        InventoryDTO dto = super.toDTO(entity);
        dto.setModel(new ModelDTO());
        BeanUtils.copyProperties(entity.getModel(), dto.getModel());
        return dto;
    }

    @Override
    public void updateEntity(InventoryDTO dto, Inventory entity) {

    }

    @Override
    public InventoryDTO inbound(Long modelId, BigDecimal amount) {

        Assert.notNull(modelId, "model id should not be null");
        Assert.notNull(amount, "amount should not be null");

        Model modelProbe = new Model();
        modelProbe.setId(modelId);

        Inventory probe = new Inventory();
        probe.setModel(modelProbe);

        Optional<Inventory> any = this.repository.findOne(Example.of(probe));

        if (any.isPresent()) {
            Inventory inventory = any.get();
            inventory.setQuantity(inventory.getQuantity().add(amount));
            this.repository.saveAndFlush(inventory);
            return toDTO(inventory);
        }

        throw new RuntimeException("Model Not Found");
    }

    @Override
    public InventoryDTO outbound(Long modelId, BigDecimal amount) {

        Assert.notNull(modelId, "model id should not be null");
        Assert.notNull(amount, "amount should not be null");

        Model modelProbe = new Model();
        modelProbe.setId(modelId);

        Inventory probe = new Inventory();
        probe.setModel(modelProbe);

        Optional<Inventory> any = this.repository.findOne(Example.of(probe));

        if (any.isPresent()) {
            Inventory inventory = any.get();

            if (inventory.getQuantity().compareTo(BigDecimal.ZERO) > 0 &&
                    inventory.getQuantity().subtract(amount).compareTo(BigDecimal.ZERO) >= 0
            ) {
                inventory.setQuantity(inventory.getQuantity().subtract(amount));
                this.repository.saveAndFlush(inventory);
                return toDTO(inventory);
            } else {
                throw new RuntimeException("Out of stock of model " + inventory.getModel().getName());
            }
        }

        throw new RuntimeException("Model Not Found");
    }
}
