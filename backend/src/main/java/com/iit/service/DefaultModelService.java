package com.iit.service;

import com.iit.domain.entity.Inventory;
import com.iit.domain.entity.Model;
import com.iit.domain.repository.ModelRepository;
import com.iit.service.base.AbstractStatelessJPAService;
import com.iit.service.dto.InventoryDTO;
import com.iit.service.dto.ModelDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @author william
 */
@Service
public class DefaultModelService extends AbstractStatelessJPAService<Long, ModelDTO, Model, ModelRepository> implements ModelService {

    @Override
    public ModelDTO toDTO(Model entity) {
        ModelDTO dto = super.toDTO(entity);
        dto.setInventory(new InventoryDTO());
        BeanUtils.copyProperties(entity.getInventory(), dto.getInventory());
        return dto;
    }

    @Override
    protected void createHandler(ModelDTO dto, Model entity) {

        Inventory inventory = new Inventory();
        inventory.setQuantity(BigDecimal.ZERO);

        entity.setInventory(inventory);
    }

    @Override
    public void updateEntity(ModelDTO dto, Model entity) {

    }
}
