package com.iit.service;

import com.iit.domain.entity.Customer;
import com.iit.domain.entity.Employee;
import com.iit.domain.entity.Model;
import com.iit.domain.entity.Order;
import com.iit.domain.entity.Revenue;
import com.iit.domain.repository.CustomerRepository;
import com.iit.domain.repository.EmployeeRepository;
import com.iit.domain.repository.ModelRepository;
import com.iit.domain.repository.OrderRepository;
import com.iit.domain.repository.RevenueRepository;
import com.iit.service.base.AbstractStatelessJPAService;
import com.iit.service.dto.OrderDTO;
import com.iit.util.DateUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author william
 */
@Service
public class DefaultOrderService extends AbstractStatelessJPAService<Long, OrderDTO, Order, OrderRepository> implements OrderService {

    @Resource
    private ModelRepository modelRepository;

    @Resource
    private EmployeeRepository employeeRepository;

    @Resource
    private CustomerRepository customerRepository;

    @Resource
    private RevenueRepository revenueRepository;

    @Resource
    private InventoryService inventoryService;

    @Override
    public void updateEntity(OrderDTO dto, Order entity) {

    }

    @Override
    protected void createHandler(OrderDTO dto, Order entity) {

        // outbound first to make sure stock enough
        inventoryService.outbound(dto.getModelId(), dto.getAmount());

        Model model = modelRepository.getOne(dto.getModelId());
        Employee employee = employeeRepository.getOne(dto.getEmployeeId());
        Customer customer = customerRepository.getOne(dto.getCustomerId());

        entity.setModel(model);
        entity.setEmployee(employee);
        entity.setCustomer(customer);
        entity.setSaleDate(new Date());
        entity.setOrderNo("O-" + DateUtils.FIXED_SDF.format(new Date()));
    }

    @Override
    protected void afterCreateHandler(OrderDTO dto, Order target) {
        // update report
        Revenue revenue = new Revenue();

        revenue.setAmount(target.getSaleValue());
        revenue.setCustomerId(target.getCustomer().getId());
        revenue.setEmployeeId(target.getEmployee().getId());
        revenue.setDate(target.getSaleDate());

        this.revenueRepository.saveAndFlush(revenue);
    }
}
