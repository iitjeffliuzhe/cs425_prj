package com.iit.service;

import com.iit.domain.entity.Employee;
import com.iit.domain.repository.EmployeeRepository;
import com.iit.service.base.AbstractStatelessJPAService;
import com.iit.service.dto.EmployeeDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author william
 */
@Service
@Transactional
public class DefaultPassportService extends AbstractStatelessJPAService<Long, EmployeeDTO, Employee, EmployeeRepository> implements PassportService {

    @Override
    public EmployeeDTO toDTO(Employee entity) {
        return null;
    }

    @Override
    public Employee toEntity(EmployeeDTO dto) {
        return null;
    }

    @Override
    public void updateEntity(EmployeeDTO dto, Employee entity) {

    }
}
