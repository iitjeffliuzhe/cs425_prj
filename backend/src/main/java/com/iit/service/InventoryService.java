package com.iit.service;

import com.iit.service.base.CRUDService;
import com.iit.service.dto.InventoryDTO;

import java.math.BigDecimal;

/**
 * @author william
 */
public interface InventoryService extends CRUDService<Long, InventoryDTO> {

    InventoryDTO inbound(Long modelId, BigDecimal amount);

    InventoryDTO outbound(Long modelId, BigDecimal amount);
}
