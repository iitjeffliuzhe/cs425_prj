package com.iit.service;

import com.iit.service.base.CRUDService;
import com.iit.service.dto.ModelDTO;

/**
 * @author william
 */
public interface ModelService extends CRUDService<Long, ModelDTO> {
}
