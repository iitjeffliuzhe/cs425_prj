package com.iit.service;

import com.iit.service.base.CRUDService;
import com.iit.service.dto.OrderDTO;

/**
 * @author william
 */
public interface OrderService extends CRUDService<Long, OrderDTO> {
}
