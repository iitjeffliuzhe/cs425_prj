package com.iit.service;

import com.iit.service.base.CRUDService;
import com.iit.service.dto.EmployeeDTO;

/**
 * @author william
 */
public interface PassportService extends CRUDService<Long, EmployeeDTO> {

}
