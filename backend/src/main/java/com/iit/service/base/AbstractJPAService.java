package com.iit.service.base;

import com.iit.domain.entity.AbstractPersistableEntity;
import com.iit.service.dto.IdentifierAwareDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

/**
 * @author william
 */
@Slf4j
public abstract class AbstractJPAService<ID extends Serializable, DTO extends IdentifierAwareDTO<ID>, ENTITY extends AbstractPersistableEntity<ID>, DAO extends JpaRepository<ENTITY, ID>>
        extends AbstractStatelessJPAService<ID, DTO, ENTITY, DAO> {


    @Override
    protected void createHandler(DTO dto, ENTITY entity) {
    }

    @Override
    protected void afterCreateHandler(DTO dto, ENTITY target) {
    }

    @Override
    protected void updateHandler(ENTITY target) {
    }

    @Override
    protected void deleteHandler(ENTITY target) {
    }
}
