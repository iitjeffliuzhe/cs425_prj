package com.iit.service.base;

import com.iit.util.ReflectionUtils;
import com.iit.service.dto.IdentifierAwareDTO;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;

import java.io.Serializable;

/**
 * @author william
 */
@Slf4j
public abstract class AbstractService<ID extends Serializable, DTO extends IdentifierAwareDTO> implements BeanFactoryAware, CRUDService<ID, DTO> {

    @Setter
    protected BeanFactory beanFactory;

    protected Class<DTO> dtoClass;

    protected AbstractService() {
        dtoClass = ReflectionUtils.getClassGenericType(getClass(), 1);
    }

    @Override
    public DTO create(DTO dto) {
        return null;
    }

    @Override
    public DTO retrieve(ID id) {
        return null;
    }

    @Override
    public DTO update(DTO dto) {
        return null;
    }

    @Override
    public boolean delete(ID id) {
        return false;
    }

    @Override
    public void deleteAll() {
    }

    @Override
    public boolean active(ID id) {
        return false;
    }

    @Override
    public boolean deactive(ID id) {
        return false;
    }
}
