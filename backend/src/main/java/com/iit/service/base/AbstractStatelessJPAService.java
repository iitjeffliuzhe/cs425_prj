package com.iit.service.base;

import com.iit.domain.entity.AbstractPersistableEntity;
import com.iit.service.dto.IdentifierAwareDTO;
import com.iit.util.ReflectionUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import javax.persistence.EntityNotFoundException;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static org.springframework.beans.BeanUtils.copyProperties;

/**
 * @author william
 */
@Slf4j
public abstract class AbstractStatelessJPAService<ID extends Serializable, DTO extends IdentifierAwareDTO<ID>, ENTITY extends AbstractPersistableEntity<ID>, REPOSITORY extends JpaRepository<ENTITY, ID>>
        extends AbstractService<ID, DTO> implements PageableService<ID, DTO>, ObjectConverter<ID, DTO, ENTITY> {

    protected Class<DTO> dtoClass;
    protected Class<ENTITY> entityClass;
    protected Class<REPOSITORY> repositoryClass;

    protected REPOSITORY repository;

    protected boolean allowingDelAll = false;

    Map<String, Field> relativeFields = new ConcurrentHashMap<>();
    Map<String, JpaRepository<? extends AbstractPersistableEntity<ID>, ID>> relativeRepositories = new ConcurrentHashMap<>();

    private final static ExampleMatcher DEFAULT_STRING_MATCHER = ExampleMatcher.matching().withStringMatcher
            (ExampleMatcher.StringMatcher.STARTING);

    @SuppressWarnings("unchecked")
    public AbstractStatelessJPAService() {
        super();
        dtoClass = ReflectionUtils.getClassGenericType(getClass(), 1);
        entityClass = ReflectionUtils.getClassGenericType(getClass(), 2);
        repositoryClass = ReflectionUtils.getClassGenericType(getClass(), 3);
    }

    protected void createHandler(DTO dto, ENTITY entity) {
    }

    protected void afterCreateHandler(DTO dto, ENTITY target) {
    }

    protected void updateHandler(ENTITY target) {
    }

    protected void deleteHandler(ENTITY target) {
    }

    protected void merge(ENTITY target) {
    }

    protected void cleanup(ENTITY target) {
    }

    @SneakyThrows
    @Override
    public DTO toDTO(ENTITY entity) {
        DTO dto = dtoClass.newInstance();
        copyProperties(entity, dto);
        return dto;
    }

    @SneakyThrows
    @Override
    public ENTITY toEntity(DTO dto) {
        ENTITY entity = entityClass.newInstance();
        copyProperties(dto, entity);
        return entity;
    }

    @Transactional
    public DTO create(DTO dto) {
        ENTITY entity = toEntity(dto);
        createHandler(dto, entity);
        repository.save(entity);
        afterCreateHandler(dto, entity);
        return toDTO(entity);
    }

    @Transactional(readOnly = true)
    public DTO retrieve(ID id) {

        try {
            return toDTO(repository.getOne(id));
        } catch (EntityNotFoundException e) {
            log.warn("retrieve entity not found --- id : {} ", id);
            return null;
        }
    }

    @Transactional
    public DTO update(DTO dto) {
        Assert.notNull(dto.getId(), "update operation must has id");

        try {
            ENTITY target = repository.getOne(dto.getId());
            cleanup(target);
            updateEntity(dto, target);
            updateHandler(target);
            merge(target);
            return toDTO(repository.save(target));
        } catch (EntityNotFoundException e) {
            log.warn("update entity not successful --- id : {} ", dto.getId());
            return null;
        }
    }

    @Transactional
    public boolean delete(ID id) {

        try {
            ENTITY target = repository.getOne(id);
            this.physicDelete(target);
            return true;
        } catch (EntityNotFoundException e) {
            log.warn("delete entity not successful --- id : {} ", id);
            return false;
        }
    }

    @Transactional
    public void deleteAll() {

        if (!allowingDelAll) {
            return;
        }

        List<ENTITY> all = repository.findAll();

        for (ENTITY entity : all) {
            this.delete(entity.getId());
        }
    }

    private boolean logicDelete(ENTITY entity) {
        deleteHandler(entity);
        return true;
    }

    private boolean physicDelete(ENTITY entity) {
        repository.delete(entity);
        return true;
    }

    @Transactional
    public boolean active(ID id) {
        return this.toggleActive(id, true);
    }

    @Transactional
    public boolean deactive(ID id) {
        return this.toggleActive(id, false);
    }

    private boolean toggleActive(ID id, boolean active) {

//        if (!dao.exists(id)) {
//            log.warn("entity not found --- id : {} ", id);
//            return false;
//        }
//
//        ENTITY entity = dao.findOne(id);
//        entity.setActive(active);
        return true;
    }

    @Transactional(readOnly = true)
    public Page<DTO> listActive(Pageable pageable) {

        ENTITY probe = newProbeInstance();

        Example<ENTITY> example = Example.of(probe);
        Page<DTO> data = repository.findAll(example, pageable).map(entity -> toDTO(entity));
        return data;
    }

    @Transactional(readOnly = true)
    public List<DTO> listAllActive() {

        ENTITY probe = newProbeInstance();

        Example<ENTITY> example = Example.of(probe);
        List<DTO> data = repository.findAll(example).stream().map(entity -> toDTO(entity)).collect(Collectors.toList());
        return data;
    }

    private ENTITY newProbeInstance() {
        ENTITY probe = null;
        try {
            probe = entityClass.newInstance();
        } catch (IllegalAccessException | InstantiationException e) {
            log.error("this is not gonna happen. {}", e);
        }
        return probe;
    }

    @Transactional(readOnly = true)
    public Page<DTO> listAll(Pageable pageable) {
        Page<DTO> data = repository.findAll(pageable).map(entity -> toDTO(entity));
        return data;
    }

    @Transactional(readOnly = true)
    public Page<DTO> list(DTO condition, Pageable pageable) {

        ENTITY prob = toEntity(condition);
        Example<ENTITY> example = Example.of(prob, DEFAULT_STRING_MATCHER);

        Page<DTO> data = repository.findAll(example, pageable).map(entity -> toDTO(entity));
        return data;
    }

    @Deprecated
    public <U> List<DTO> list(U u) {
        throw new RuntimeException("not support method");
    }

    @PostConstruct
    private void assignmentDao() {
        this.repository = beanFactory.getBean(repositoryClass);

        for (Field field : entityClass.getDeclaredFields()) {
            if (field.isAnnotationPresent(ManyToOne.class) || field.isAnnotationPresent(OneToOne.class)) {
                JpaRepository<? extends AbstractPersistableEntity<ID>, ID> relativeDao = (JpaRepository<? extends
                        AbstractPersistableEntity<ID>, ID>) beanFactory.getBean(decapitalize(field
                        .getType().getSimpleName() + "Repository"));

                relativeFields.put(field.getName(), field);
                relativeRepositories.put(field.getName(), relativeDao);
            }

            if (field.isAnnotationPresent(OneToMany.class) || field.isAnnotationPresent(ManyToMany.class)) {

                String childEntityName = ((Class) (((ParameterizedType) field.getGenericType()).getActualTypeArguments()
                        [0]))
                        .getSimpleName();
                JpaRepository<? extends AbstractPersistableEntity<ID>, ID> relativeDao = (JpaRepository<? extends
                        AbstractPersistableEntity<ID>, ID>) beanFactory.getBean(decapitalize(childEntityName + "Repository"));

                relativeFields.put(field.getName(), field);
                relativeRepositories.put(field.getName(), relativeDao);
            }
        }
    }

    private static String decapitalize(String string) {
        if (string == null || string.length() == 0) {
            return string;
        }
        char c[] = string.toCharArray();
        c[0] = Character.toLowerCase(c[0]);
        return new String(c);
    }
}
