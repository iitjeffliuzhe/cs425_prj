package com.iit.service.base;

/**
 * @author william
 */
public interface CRUDService<ID, DTO> extends BaseService<ID, DTO> {

    DTO create(DTO dto);

    DTO retrieve(ID id);

    DTO update(DTO dto);

    boolean delete(ID id);

    void deleteAll();

    boolean active(ID id);

    boolean deactive(ID id);
}