package com.iit.service.base;

/**
 * @author william
 */
public interface CRUDServiceAware<S> {

    <SS extends S> SS getService();
}
