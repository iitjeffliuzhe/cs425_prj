package com.iit.service.base;

import com.iit.domain.entity.AbstractPersistableEntity;
import com.iit.service.dto.IdentifierAwareDTO;

import java.io.Serializable;

/**
 * @author william
 */
public interface ObjectConverter<ID extends Serializable, DTO extends IdentifierAwareDTO, ENTITY extends AbstractPersistableEntity<ID>> {

    DTO toDTO(ENTITY entity);

    ENTITY toEntity(DTO dto);

    void updateEntity(DTO dto, ENTITY entity);
}
