package com.iit.service.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class AddressDTO implements IdentifierAwareDTO<Long> {

    private Long id;

    private String streetName;

    private String streetNo;

    private String aptNo;

    private String city;

    private String state;

    private BigDecimal zip;
}