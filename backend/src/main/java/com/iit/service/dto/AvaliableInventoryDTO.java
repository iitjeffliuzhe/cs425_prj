package com.iit.service.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class AvaliableInventoryDTO implements IdentifierAwareDTO<Long> {

    private Long id;

    private BigDecimal number;

    private BigDecimal amount;

    private Timestamp date;
}