package com.iit.service.dto;

import lombok.Data;

@Data
public class CustomerDTO implements IdentifierAwareDTO<Long> {

    private Long id;

    private String firstName;

    private String lastName;

    private String phoneNo;

    private Long addressId;
}