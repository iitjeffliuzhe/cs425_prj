package com.iit.service.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class EmployeeDTO implements IdentifierAwareDTO<Long> {

    private Long id;

    private String firstName;

    private String lastName;

    private String ssn;

    private BigDecimal salary;

    private String userName;

    private String password;

    private Integer type;

    private Long addressId;

    private Long roleId;
}