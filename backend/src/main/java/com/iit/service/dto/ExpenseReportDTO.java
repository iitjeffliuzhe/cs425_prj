package com.iit.service.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class ExpenseReportDTO implements IdentifierAwareDTO<Long> {

    private Long id;

    private String expenseType;

    private BigDecimal cost;

    private Timestamp date;
}