package com.iit.service.dto;

/**
 * @author william
 */
public interface IdentifierAwareDTO<ID> {

    ID getId();

    void setId(ID id);
}
