package com.iit.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class InventoryDTO implements IdentifierAwareDTO<Long> {

    private Long id;

    private Date lentTime;

    private String categoryType;

    private BigDecimal quantity;

    private Long modelId;

    private ModelDTO model;
}