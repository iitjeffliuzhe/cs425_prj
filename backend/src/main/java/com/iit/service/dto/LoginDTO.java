package com.iit.service.dto;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class LoginDTO implements IdentifierAwareDTO<Long> {

    private Long id;

    private Timestamp loginTime;

    private String ip;

    private Timestamp logoutTime;

    private Integer employeeId;
}