package com.iit.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ModelDTO implements IdentifierAwareDTO<Long> {

    private Long id;

    private String name;

    private String modelNo;

    private BigDecimal salePrice;

    private InventoryDTO inventory;
}