package com.iit.service.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class OrderDTO implements IdentifierAwareDTO<Long> {

    private Long id;

    private String orderNo;

    private Long customerId;

    private Long employeeId;

    private Long modelId;

    private BigDecimal saleValue;

    private Date saleDate;

    private CustomerDTO customer;

    private EmployeeDTO employee;

    private ModelDTO model;

    private BigDecimal amount;
}