package com.iit.service.dto;

import lombok.Data;

@Data
public class PremissionDTO implements IdentifierAwareDTO<Long> {

    private Long id;

    private String premissionName;
}