package com.iit.service.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class RevenueDTO implements IdentifierAwareDTO<Long> {

    private Long id;

    private Integer customerId;

    private Integer employeeId;

    private BigDecimal amount;

    private Timestamp date;
}