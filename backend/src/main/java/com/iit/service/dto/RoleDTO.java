package com.iit.service.dto;

import lombok.Data;

@Data
public class RoleDTO implements IdentifierAwareDTO<Long> {

    private Long id;

    private String name;

    private String description;
}