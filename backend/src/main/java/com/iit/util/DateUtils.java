package com.iit.util;

import java.text.SimpleDateFormat;

/**
 * @author william
 */
public final class DateUtils {

    public final static SimpleDateFormat FIXED_SDF = new SimpleDateFormat("yyyyMMddHHmmss");
}
