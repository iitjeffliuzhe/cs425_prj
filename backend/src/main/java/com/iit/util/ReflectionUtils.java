package com.iit.util;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @author william
 */
public class ReflectionUtils extends org.springframework.util.ReflectionUtils {

    @SuppressWarnings("unchecked")
    public static <T> Class<T> getClassGenericType(final Class<?> clazz) {
        return getClassGenericType(clazz, 0);
    }

    @SuppressWarnings("rawtypes")
    public static Class getClassGenericType(final Class clazz, final int index) {
        Type genType = clazz.getGenericSuperclass();
        if (!(genType instanceof ParameterizedType)) {
            return Object.class;
        }
        Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
        if (index >= params.length || index < 0) {
            return Object.class;
        }
        if (!(params[index] instanceof Class)) {
            return Object.class;
        }
        return (Class) params[index];
    }
}
