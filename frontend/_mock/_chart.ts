// tslint:disable
import * as Mock from 'mockjs';
import { format } from 'date-fns';
import { deepCopy } from '@delon/util';

// region: mock data

const visitData: any[] = [];
const beginDay = new Date().getTime();

const fakeY = [4, 5, 4, 6, 5, 7, 5, 6, 5, 9, 6, 3, 1, 5, 3, 6, 5];
for (let i = 0; i < fakeY.length; i += 1) {
  visitData.push({
    x: format(new Date(beginDay + 1000 * 60 * 60 * 24 * i), 'YYYY-MM-DD'),
    y: fakeY[i],
  });
}

const visitData2: any[] = [];
const fakeY2 = [1, 6, 4, 8, 3, 7, 2];
for (let i = 0; i < fakeY2.length; i += 1) {
  visitData2.push({
    x: format(new Date(beginDay + 1000 * 60 * 60 * 24 * i), 'YYYY-MM-DD'),
    y: fakeY2[i],
  });
}

const salesData: any[] = [
  { x: '1月', y: 633.393 },
  { x: '2月', y: 640.106 },
  { x: '3月', y: 1157.076 },
  { x: '4月', y: 1595.176 },
  { x: '5月', y: 1442.254 },
  { x: '6月', y: 1848.44 },
  { x: '7月', y: 1454.798 },
  { x: '8月', y: 1744.704 },
  { x: '9月', y: 1577.899 },
  { x: '10月', y: 1470.121 },
  { x: '11月', y: 1482.231 },
  { x: '12月', y: 1101.906 },
];
// for (let i = 0; i < 12; i += 1) {
//   salesData.push({
//     x: `${i + 1}月`,
//     y: Math.floor(Math.random() * 600) + 1000,
//   });
// }
const searchData: any[] = [];
for (let i = 0; i < 50; i += 1) {
  searchData.push({
    index: i + 1,
    keyword: `搜索关键词-${i}`,
    count: Math.floor(Math.random() * 1000),
    range: Math.floor(Math.random() * 100),
    status: Math.floor((Math.random() * 10) % 2),
  });
}
const salesTypeData = [
  {
    x: '家用电器',
    y: 4544,
  },
  {
    x: '食用酒水',
    y: 3321,
  },
  {
    x: '个护健康',
    y: 3113,
  },
  {
    x: '服饰箱包',
    y: 2341,
  },
  {
    x: '母婴产品',
    y: 1231,
  },
  {
    x: '其他',
    y: 1231,
  },
];

const salesTypeDataOnline = [
  {
    x: '家用电器',
    y: 244,
  },
  {
    x: '食用酒水',
    y: 321,
  },
  {
    x: '个护健康',
    y: 311,
  },
  {
    x: '服饰箱包',
    y: 41,
  },
  {
    x: '母婴产品',
    y: 121,
  },
  {
    x: '其他',
    y: 111,
  },
];

const salesTypeDataOffline = [
  {
    x: '家用电器',
    y: 99,
  },
  {
    x: '个护健康',
    y: 188,
  },
  {
    x: '服饰箱包',
    y: 344,
  },
  {
    x: '母婴产品',
    y: 255,
  },
  {
    x: '其他',
    y: 65,
  },
];

// const offlineData: any[] = [];
// for (let i = 0; i < 10; i += 1) {
//   offlineData.push({
//     name: `电站${i}`,
//     cvr: Math.ceil(Math.random() * 9) / 10,
//   });
// }
const offlineData: any[] = [
  {
    name: '联发',
    cvr: 0.64,
  },
  {
    name: '亿仕登',
    cvr: 0.9,
  },
  {
    name: '康宁',
    cvr: 0.95,
  },
  {
    name: '奥英',
    cvr: 0.96,
  },
  {
    name: '锦富',
    cvr: 0.72,
  },
  {
    name: '苏净',
    cvr: 0.93,
  },
  {
    name: '雨润',
    cvr: 0.71,
  },
  {
    name: '爱德',
    cvr: 0.98,
  },
  {
    name: '耐火材料',
    cvr: 1,
  },
  {
    name: '柳工',
    cvr: 0.87,
  },
  {
    name: '洋河',
    cvr: 1,
  },
];
// const offlineChartData: any[] = [];
// for (let i = 0; i < 20; i += 1) {
//   offlineChartData.push({
//     x: new Date().getTime() + 1000 * 60 * 30 * i,
//     y1: Math.floor(Math.random() * 100) + 10,
//     y2: Math.floor(Math.random() * 100) + 10,
//   });
// }
const offlineChartData: any[] = [
  {
    x: '2020-01-01 00:00:00',
    y1: 839.4,
    y2: 56,
  },
  {
    x: '2020-02-01 00:00:00',
    y1: 846.11,
    y2: 0,
  },
  {
    x: '2020-03-01 00:00:00',
    y1: 1363,
    y2: 0,
  },
  {
    x: '2020-04-01 00:00:00',
    y1: 1801.18,
    y2: 0,
  },
  {
    x: '2020-05-01 00:00:00',
    y1: 1604.3,
    y2: 0,
  },
  {
    x: '2020-06-01 00:00:00',
    y1: 2010.5,
    y2: 0,
  },
  {
    x: '2020-07-01 00:00:00',
    y1: 1616,
    y2: 0,
  },
  {
    x: '2020-08-01 00:00:00',
    y1: 1906,
    y2: 0,
  },
  {
    x: '2020-09-01 00:00:00',
    y1: 1739,
    y2: 0,
  },
  {
    x: '2020-10-01 00:00:00',
    y1: 1470,
    y2: 0,
  },
  {
    x: '2020-11-01 00:00:00',
    y1: 1482,
    y2: 0,
  },
  {
    x: '2020-12-01 00:00:00',
    y1: 1101,
    y2: 0,
  },
];

const radarOriginData = [
  {
    name: '个人',
    ref: 10,
    koubei: 8,
    output: 4,
    contribute: 5,
    hot: 7,
  },
  {
    name: '团队',
    ref: 3,
    koubei: 9,
    output: 6,
    contribute: 3,
    hot: 1,
  },
  {
    name: '部门',
    ref: 4,
    koubei: 1,
    output: 6,
    contribute: 5,
    hot: 7,
  },
];

//
const radarData: any[] = [];
const radarTitleMap = {
  ref: '引用',
  koubei: '口碑',
  output: '产量',
  contribute: '贡献',
  hot: '热度',
};
radarOriginData.forEach(item => {
  Object.keys(item).forEach(key => {
    if (key !== 'name') {
      radarData.push({
        name: item.name,
        label: radarTitleMap[key],
        value: item[key],
      });
    }
  });
});

const statisticData = {
  elecTotal: 16148.104,
};
// endregion

export const CHARTS = {
  '/chart': JSON.parse(
    JSON.stringify({
      visitData,
      visitData2,
      salesData,
      searchData,
      offlineData,
      offlineChartData,
      salesTypeData,
      salesTypeDataOnline,
      salesTypeDataOffline,
      radarData,
      statisticData,
    }),
  ),
  '/chart/visit': JSON.parse(JSON.stringify(visitData)),
  '/chart/tags': Mock.mock({
    'list|100': [{ x: '@city', 'value|1-100': 150, 'category|0-2': 1 }],
  }),
};
