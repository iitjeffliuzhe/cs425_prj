// tslint:disable
import * as Mock from 'mockjs';
import { format } from 'date-fns';
import { deepCopy } from '@delon/util';
import { MockRequest } from '@delon/mock';

// region: mock data

const visitData: any[] = [];
const beginDay = new Date().getTime();

const fakeY = [4, 5, 4, 6, 5, 7, 5, 6, 5, 9, 6, 3, 1, 5, 3, 6, 5];
for (let i = 0; i < fakeY.length; i += 1) {
  visitData.push({
    x: format(new Date(beginDay + 1000 * 60 * 60 * 24 * i), 'YYYY-MM-DD'),
    y: fakeY[i],
  });
}

const visitData2: any[] = [];
const fakeY2 = [1, 6, 4, 8, 3, 7, 2];
for (let i = 0; i < fakeY2.length; i += 1) {
  visitData2.push({
    x: format(new Date(beginDay + 1000 * 60 * 60 * 24 * i), 'YYYY-MM-DD'),
    y: fakeY2[i],
  });
}

const salesData: any[] = [
  { x: '1月', y: 633.393 },
  { x: '2月', y: 640.106 },
  { x: '3月', y: 1157.076 },
  { x: '4月', y: 1595.176 },
  { x: '5月', y: 1442.254 },
  { x: '6月', y: 1848.44 },
  { x: '7月', y: 1454.798 },
  { x: '8月', y: 1744.704 },
  { x: '9月', y: 1577.899 },
  { x: '10月', y: 1470.121 },
  { x: '11月', y: 1482.231 },
  { x: '12月', y: 1101.906 },
];
// for (let i = 0; i < 12; i += 1) {
//   salesData.push({
//     x: `${i + 1}月`,
//     y: Math.floor(Math.random() * 600) + 1000,
//   });
// }
const searchData: any[] = [];
for (let i = 0; i < 50; i += 1) {
  searchData.push({
    index: i + 1,
    keyword: `搜索关键词-${i}`,
    count: Math.floor(Math.random() * 1000),
    range: Math.floor(Math.random() * 100),
    status: Math.floor((Math.random() * 10) % 2),
  });
}
const salesTypeData = [
  {
    x: '家用电器',
    y: 4544,
  },
  {
    x: '食用酒水',
    y: 3321,
  },
  {
    x: '个护健康',
    y: 3113,
  },
  {
    x: '服饰箱包',
    y: 2341,
  },
  {
    x: '母婴产品',
    y: 1231,
  },
  {
    x: '其他',
    y: 1231,
  },
];

const salesTypeDataOnline = [
  {
    x: '家用电器',
    y: 244,
  },
  {
    x: '食用酒水',
    y: 321,
  },
  {
    x: '个护健康',
    y: 311,
  },
  {
    x: '服饰箱包',
    y: 41,
  },
  {
    x: '母婴产品',
    y: 121,
  },
  {
    x: '其他',
    y: 111,
  },
];

const salesTypeDataOffline = [
  {
    x: '家用电器',
    y: 99,
  },
  {
    x: '个护健康',
    y: 188,
  },
  {
    x: '服饰箱包',
    y: 344,
  },
  {
    x: '母婴产品',
    y: 255,
  },
  {
    x: '其他',
    y: 65,
  },
];

// const offlineData: any[] = [];
// for (let i = 0; i < 10; i += 1) {
//   offlineData.push({
//     name: `电站${i}`,
//     cvr: Math.ceil(Math.random() * 9) / 10,
//   });
// }
const offlineData: any[] = [
  {
    name: '联发',
    cvr: 0.64,
  },
  {
    name: '亿仕登',
    cvr: 0.9,
  },
  {
    name: '康宁',
    cvr: 0.95,
  },
  {
    name: '奥英',
    cvr: 0.96,
  },
  {
    name: '锦富',
    cvr: 0.72,
  },
  {
    name: '苏净',
    cvr: 0.93,
  },
  {
    name: '雨润',
    cvr: 0.71,
  },
  {
    name: '爱德',
    cvr: 0.98,
  },
  {
    name: '耐火材料',
    cvr: 1,
  },
  {
    name: '柳工',
    cvr: 0.87,
  },
  {
    name: '洋河',
    cvr: 1,
  },
];
// const offlineChartData: any[] = [];
// for (let i = 0; i < 20; i += 1) {
//   offlineChartData.push({
//     x: new Date().getTime() + 1000 * 60 * 30 * i,
//     y1: Math.floor(Math.random() * 100) + 10,
//     y2: Math.floor(Math.random() * 100) + 10,
//   });
// }
const offlineChartData: any[] = [
  {
    x: '2020-01-01 00:00:00',
    y1: 66.9,
    y2: 33,
  },
  {
    x: '2020-02-01 00:00:00',
    y1: 71.8,
    y2: 0,
  },
  {
    x: '2020-03-01 00:00:00',
    y1: 86.8,
    y2: 0,
  },
  {
    x: '2020-04-01 00:00:00',
    y1: 102.8,
    y2: 0,
  },
  {
    x: '2020-05-01 00:00:00',
    y1: 117,
    y2: 0,
  },
  {
    x: '2020-06-01 00:00:00',
    y1: 102.8,
    y2: 0,
  },
  {
    x: '2020-07-01 00:00:00',
    y1: 124.7,
    y2: 0,
  },
  {
    x: '2020-08-01 00:00:00',
    y1: 120.7,
    y2: 0,
  },
  {
    x: '2020-09-01 00:00:00',
    y1: 102.8,
    y2: 0,
  },
  {
    x: '2020-10-01 00:00:00',
    y1: 94.8,
    y2: 0,
  },
  {
    x: '2020-11-01 00:00:00',
    y1: 73.8,
    y2: 0,
  },
  {
    x: '2020-12-01 00:00:00',
    y1: 67.9,
    y2: 0,
  },
];

const radarOriginData = [
  {
    name: '个人',
    ref: 10,
    koubei: 8,
    output: 4,
    contribute: 5,
    hot: 7,
  },
  {
    name: '团队',
    ref: 3,
    koubei: 9,
    output: 6,
    contribute: 3,
    hot: 1,
  },
  {
    name: '部门',
    ref: 4,
    koubei: 1,
    output: 6,
    contribute: 5,
    hot: 7,
  },
];

//
const radarData: any[] = [];
const radarTitleMap = {
  ref: '引用',
  koubei: '口碑',
  output: '产量',
  contribute: '贡献',
  hot: '热度',
};
radarOriginData.forEach(item => {
  Object.keys(item).forEach(key => {
    if (key !== 'name') {
      radarData.push({
        name: item.name,
        label: radarTitleMap[key],
        value: item[key],
      });
    }
  });
});

const statisticData = {
  elecTotal: 16148.104,
};
// endregion

const stationsData = {
  WDSOLAR_VP_002: {
    stationName: '苏净',
    totalPower: 610.4, //总发电量 Mwh
    dayPower: 1672, //日发电量 kWh
    monthPower: 22069, //月发电量 kwh
    totalIncome: 37.85, //累计收益，万元
    dayIncome: 1036, //日收益，元
    monthIncome: 13682, //月收益，元
    totalHour: 1121, //累计发电时长
    dayHour: 3.2, //日发电时长
    monthHour: 42, //月发电时长
    usageRate: 93.64, //自发自用比例
    basicInfo: {
      capacity: 534.6,
      buildTime: '2018.06.21',
      fullName: '江苏苏净集团有限公司屋顶分布式光伏发电项目',
      address: '苏州工业园区中心科技城',
      contactPerson: '袁开军',
      gridVoltage: 0.4,
      reduceCO2: 409.39,
      weather: 'http://weather.seniverse.com/?token=f34a5830-8cfa-46db-932a-1b911101fb37',
    },
    deviceInfo: [
      {
        deviceName: '光伏组件',
        brand: '阿特斯',
        manufactor: 'CS6K-270P',
        count: 1980,
      },
      {
        deviceName: '逆变器',
        brand: '华为',
        manufactor: 'SUN2000-36KTL',
        count: 7,
      },
      {
        deviceName: '逆变器',
        brand: '华为',
        manufactor: 'SUN2000-60KTL',
        count: 4,
      },
      {
        deviceName: '开关柜',
        brand: '订制',
        manufactor: '汇流箱3台，并网柜1台',
        count: 4,
      },
    ],
  },
  WDSOLAR_VP_003: {
    stationName: '联发',
    totalPower: 1166.16, //总发电量 Mwh
    dayPower: 3194.9, //日发电量 kWh
    monthPower: 43822, //月发电量 kwh
    totalIncome: 72.3, //累计收益，万元
    dayIncome: 1980, //日收益，元
    monthIncome: 27169, //月收益，元
    totalHour: 1119, //累计发电时长
    dayHour: 3.1, //日发电时长
    monthHour: 40, //月发电时长
    usageRate: 65.54, //自发自用比例
    basicInfo: {
      capacity: 534.6,
      buildTime: '2018.06.21',
      fullName: '江苏苏净集团有限公司屋顶分布式光伏发电项目',
      address: '苏州工业园区中心科技城',
      contactPerson: '袁开军',
      gridVoltage: 0.4,
      reduceCO2: 409.39,
      weather: 'http://weather.seniverse.com/?token=f34a5830-8cfa-46db-932a-1b911101fb37',
    },
    deviceInfo: [
      {
        deviceName: '光伏组件',
        brand: '阿特斯',
        manufactor: 'CS6K-270P',
        count: 1980,
      },
      {
        deviceName: '逆变器',
        brand: '华为',
        manufactor: 'SUN2000-36KTL',
        count: 7,
      },
      {
        deviceName: '逆变器',
        brand: '华为',
        manufactor: 'SUN2000-60KTL',
        count: 4,
      },
      {
        deviceName: '开关柜',
        brand: '订制',
        manufactor: '汇流箱3台，并网柜1台',
        count: 4,
      },
    ],
  },
  WDSOLAR_VP_004: {
    stationName: '奥英',
    totalPower: 968.55, //总发电量 Mwh
    dayPower: 2653.2, //日发电量 kWh
    monthPower: 35552, //月发电量 kwh
    totalIncome: 60.05, //累计收益，万元
    dayIncome: 1645.2, //日收益，元
    monthIncome: 27169, //月收益，元
    totalHour: 1148, //累计发电时长
    dayHour: 3.1, //日发电时长
    monthHour: 42, //月发电时长
    usageRate: 96.21, //自发自用比例
    basicInfo: {
      capacity: 864.43,
      buildTime: '2018.02.02',
      fullName: '苏州奥英分布奥英光电（苏州）有限公司屋顶分布式光伏发电项目式光伏发电项目',
      address: '苏州市苏州工业园区金田路15号',
      contactPerson: '袁开军',
      gridVoltage: 0.4,
      reduceCO2: 661.97,
      weather: 'http://weather.seniverse.com/?token=f34a5830-8cfa-46db-932a-1b911101fb37',
    },
    deviceInfo: [
      {
        deviceName: '光伏组件',
        brand: '英利',
        manufactor: 'YL260P-29b',
        count: 3322,
      },
      {
        deviceName: '逆变器',
        brand: '阳光',
        manufactor: 'SG60KTL',
        count: 15,
      },
      {
        deviceName: '开关柜',
        brand: '订制',
        manufactor: '汇流箱4台，并网柜2台',
        count: 6,
      },
    ],
  },
  WDSOLAR_VP_005: {
    stationName: '康宁',
    totalPower: 911.82, //总发电量 Mwh
    dayPower: 2498, //日发电量 kWh
    monthPower: 43251, //月发电量 kwh
    totalIncome: 56.53, //累计收益，万元
    dayIncome: 1548.86, //日收益，元
    monthIncome: 27169, //月收益，元
    totalHour: 1022, //累计发电时长
    dayHour: 3.25, //日发电时长
    monthHour: 40, //月发电时长
    usageRate: 95.18, //自发自用比例
    basicInfo: {
      capacity: 892.44,
      buildTime: '2019.03.06',
      fullName: '康宁生命科学(吴江)有限公司屋顶分布式光伏发电项目',
      address: '苏州市苏州工业园区金田路15号',
      contactPerson: '袁开军',
      gridVoltage: 0.4,
      reduceCO2: 683.42,
      weather: 'http://weather.seniverse.com/?token=f34a5830-8cfa-46db-932a-1b911101fb37',
    },
    deviceInfo: [
      {
        deviceName: '光伏组件',
        brand: '阿特斯',
        manufactor: 'CS6U-335P',
        count: 2664,
      },
      {
        deviceName: '逆变器',
        brand: '华为',
        manufactor: 'SUN2000-60KTL-M0',
        count: 15,
      },
      {
        deviceName: '开关柜',
        brand: '订制',
        manufactor: '汇流箱4台，并网柜2台',
        count: 6,
      },
    ],
  },
  WDSOLAR_VP_006: {
    stationName: '亿仕登',
    totalPower: 1221.53, //总发电量 Mwh
    dayPower: 3346.9, //日发电量 kWh
    monthPower: 43822, //月发电量 kwh
    totalIncome: 75.83, //累计收益，万元
    dayIncome: 2075, //日收益，元
    monthIncome: 27169, //月收益，元
    totalHour: 1215, //累计发电时长
    dayHour: 3.15, //日发电时长
    monthHour: 42.5, //月发电时长
    usageRate: 90.08, //自发自用比例
    basicInfo: {
      capacity: 1008,
      buildTime: '2018.02.07',
      fullName: '创优实业（苏州）有限公司屋顶分布式光伏发电项目',
      address: '苏州市吴江经济开发区江兴东路1128号',
      contactPerson: '袁开军',
      gridVoltage: 0.4,
      reduceCO2: 759.13,
      weather: 'http://weather.seniverse.com/?token=f34a5830-8cfa-46db-932a-1b911101fb37',
    },
    deviceInfo: [
      {
        deviceName: '光伏组件',
        brand: '中来光电',
        manufactor: 'JW-D60N-315',
        count: 3200,
      },
      {
        deviceName: '光伏组件',
        brand: '英利',
        manufactor: '英利多晶265',
        count: 330,
      },
      {
        deviceName: '逆变器',
        brand: '华为',
        manufactor: 'SUN2000-36KTL',
        count: 26,
      },
      {
        deviceName: '逆变器',
        brand: '华为',
        manufactor: 'SUN2000-17KTL',
        count: 3,
      },
      {
        deviceName: '开关柜',
        brand: '订制',
        manufactor: '汇流箱4台，并网柜2台',
        count: 6,
      },
    ],
  },
  WDSOLAR_VP_009: {
    stationName: '爱德',
    totalPower: 2097.82, //总发电量 Mwh
    dayPower: 5747, //日发电量 kWh
    monthPower: 43822, //月发电量 kwh
    totalIncome: 130.05, //累计收益，万元
    dayIncome: 3563, //日收益，元
    monthIncome: 27169, //月收益，元
    totalHour: 1044, //累计发电时长
    dayHour: 3.4, //日发电时长
    monthHour: 42, //月发电时长
    usageRate: 98.4, //自发自用比例
    basicInfo: {
      capacity: 2008.8,
      buildTime: '2017.12.26',
      fullName: '南京爱德印刷有限公司屋顶分布式光伏发电项目',
      address: '南京江宁区将军大道秣稠中路99号',
      contactPerson: '袁开军',
      gridVoltage: 10,
      reduceCO2: 1538.51,
      weather: 'http://weather.seniverse.com/?token=f34a5830-8cfa-46db-932a-1b911101fb37',
    },
    deviceInfo: [
      {
        deviceName: '光伏组件',
        brand: '阿特斯',
        manufactor: 'CS6K-270P',
        count: 7440,
      },
      {
        deviceName: '逆变器',
        brand: '华为',
        manufactor: 'SUN2000-50KTL-C1',
        count: 40,
      },
      {
        deviceName: '开关柜',
        brand: '订制',
        manufactor: '高压开关柜2台',
        count: 2,
      },
    ],
  },
  WDSOLAR_VP_010: {
    stationName: '雨润',
    totalPower: 4062.24, //总发电量 Mwh
    dayPower: 11129.42, //日发电量 kWh
    monthPower: 43822, //月发电量 kwh
    totalIncome: 251.3, //累计收益，万元
    dayIncome: 6900, //日收益，元
    monthIncome: 27169, //月收益，元
    totalHour: 1055, //累计发电时长
    dayHour: 3.3, //日发电时长
    monthHour: 43, //月发电时长
    usageRate: 71.36, //自发自用比例
    basicInfo: {
      capacity: 3850,
      buildTime: '2015.03.18',
      fullName: '南京华虹新能源科技有限公司（江苏雨润）3.85MWp分布式光伏项目',
      address: '南京市浦口区经济开发区紫峰路19号江苏雨润肉食品有限公司',
      contactPerson: '袁开军',
      gridVoltage: 10,
      reduceCO2: 2948.66,
      weather: 'http://weather.seniverse.com/?token=f34a5830-8cfa-46db-932a-1b911101fb37',
    },
    deviceInfo: [
      {
        deviceName: '光伏组件',
        brand: '林洋',
        manufactor: 'LY-Ba255p',
        count: 15100,
      },
      {
        deviceName: '光伏组件',
        brand: '英利',
        manufactor: '多晶265',
        count: 330,
      },
      {
        deviceName: '逆变器',
        brand: '兆伏艾索',
        manufactor: 'NSG-500K3TL-20',
        count: 3,
      },
      {
        deviceName: '逆变器',
        brand: '华为',
        manufactor: 'SUN2000-28KTL',
        count: 62,
      },
      {
        deviceName: '开关柜',
        brand: '订制',
        manufactor: '汇流箱15台，并网柜3台',
        count: 18,
      },
    ],
  },
  WDSOLAR_VP_011: {
    stationName: '洋河',
    totalPower: 2325, //总发电量 Mwh
    dayPower: 6370, //日发电量 kWh
    monthPower: 1111, //月发电量 kwh
    totalIncome: 144.3, //累计收益，万元
    dayIncome: 3949, //日收益，元
    monthIncome: 27051, //月收益，元
    totalHour: 1216, //累计发电时长
    dayHour: 3.25, //日发电时长
    monthHour: 41.5, //月发电时长
    usageRate: 100, //自发自用比例
    basicInfo: {
      capacity: 78.65,
      buildTime: '2018.02.02',
      fullName: '洋河基地制曲一、二车间屋顶分布式光伏发电项目',
      address: '宿迁市洋河镇中大街118号',
      contactPerson: '袁开军',
      gridVoltage: 0.4,
      reduceCO2: 1461.03,
      weather: 'http://weather.seniverse.com/?token=f34a5830-8cfa-46db-932a-1b911101fb37',
    },
    deviceInfo: [
      {
        deviceName: '光伏组件',
        brand: '尚德',
        manufactor: 'STP275-20/wfw',
        count: 286,
      },
      {
        deviceName: '逆变器',
        brand: '阳光',
        manufactor: 'SG60KTL',
        count: 2,
      },
      {
        deviceName: '开关柜',
        brand: '订制',
        manufactor: '并网柜2台',
        count: 2,
      },
    ],
  },
  WDSOLAR_VP_012: {
    stationName: '锦富',
    totalPower: 1166.16, //总发电量 Mwh
    dayPower: 3194.9, //日发电量 kWh
    monthPower: 43822, //月发电量 kwh
    totalIncome: 72.3, //累计收益，万元
    dayIncome: 1980, //日收益，元
    monthIncome: 27169, //月收益，元
    totalHour: 1111, //累计发电时长
    dayHour: 3.3, //日发电时长
    monthHour: 43, //月发电时长
    usageRate: 72.06, //自发自用比例
    basicInfo: {
      capacity: 1626.1,
      buildTime: '2018.02.02',
      fullName: '苏州锦富技术股份有限公司分布式光伏发电项目',
      address: '苏州市苏州工业园区胜浦分区江浦路39号',
      contactPerson: '袁开军',
      gridVoltage: 0.4,
      reduceCO2: 1425.3,
      weather: 'http://weather.seniverse.com/?token=f34a5830-8cfa-46db-932a-1b911101fb37',
    },
    deviceInfo: [
      {
        deviceName: '光伏组件',
        brand: '英利',
        manufactor: 'YL260P-29b',
        count: 5918,
      },
      {
        deviceName: '逆变器',
        brand: '阳光',
        manufactor: 'SG60KTL',
        count: 27,
      },
      {
        deviceName: '开关柜',
        brand: '订制',
        manufactor: '汇流箱8台，并网柜3台',
        count: 11,
      },
    ],
  },
};
const stationVideos = {
  WDSOLAR_VP_002: [],
  WDSOLAR_VP_003: [
    {
      source: 'http://hls01open.ys7.com/openlive/9a44d06067c24733963f21f643be7ab3.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/fbeeda986d2b4b1ea1dd492aa07b24f4.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/eb43ebc521d1422a8ca2b41addea7186.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/5d7eebc04b7641c997bd9677aabc45cc.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/b42d9a7440bb48a08123c10405c9dee1.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/10a542f5094c4ba89b17fcf5621fc9f3.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/2ef6f6c7732d4bc9a75a788fbc2f896f.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/12ad985ccaa84eb9847e6305d285394d.m3u8',
    },
  ],
  WDSOLAR_VP_004: [
    {
      source: 'http://hls01open.ys7.com/openlive/aafa9a15128845d5a4b365ec9f602dca.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/be02e3faeffa49f7b844f01809d96b77.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/2aa17079beb741aebb18c924fff76b36.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/35d5f4c437b14d1ea277b88425793b90.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/8a4ed7d3f3f0407cbfc003a283033177.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/f702bd983d0c42e9bc76d23d22eabc66.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/4b6db2670dd64205a2eb4c84a3c7b780.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/b447c7c8a9574681a26757c926ab6c5f.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/891397dc34104559ba69cc91087a8709.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/5dfc323a1d014dec89ab995395204975.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/d86f06b81c7e43aa99de4080fd2f7493.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/cad6cbd398c54115a74193542aba52a1.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/720fcd78f0184097a90d386998016140.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/dfff74be27b042799b6068d329ccaccb.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/faa79118b3cc44d39b57f47c8fcd2ea9.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/baf957807af84491b1aab4e92f6e8223.m3u8',
    },
  ],
  WDSOLAR_VP_005: [
    {
      source: 'http://hls01open.ys7.com/openlive/dee975b0b411455bbcaca9212dd74d97.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/698187872f0b4af48c76df8a2aaacd11.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/502a9a19e5eb49c3a5a64afb6b3b74d8.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/30653af35c7242559beebc70b16ce801.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/53a325dc7d824260be2fd940572b21b8.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/4268d28b31044bb182ad614123e015b1.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/b055579cbbba47a6b751ad7796167bab.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/1d689d50c49c47349b49addc980a2165.m3u8',
    },
  ],
  WDSOLAR_VP_006: [
    {
      source: 'http://hls01open.ys7.com/openlive/651ad9ab47204823985659888908086c.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/12d975559946407abcd82a030278c747.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/b9009f4fe3bb4793aa33992f0f092dda.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/56d7d195b578443eae934c7b509972bb.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/a7d7c16585b742dc8ebedf59b89ae9c3.m3u8',
    },
    {
      source: 'http://hls.open.ys7.com/openlive/c1c8078f07a441d99b335067f54c335c.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/262b5ddb9559458289c2925f9ab36d3d.m3u8',
    },
    {
      source: 'http://hls.open.ys7.com/openlive/000665eb81114076bdb9b59f2c2b60f6.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/501e39fbf2b04fe08c3f1e2baae53e52.m3u8',
    },
  ],
  WDSOLAR_VP_007: [
    {
      source: 'http://hls01open.ys7.com/openlive/6131289aecf2436795f23c5bb3a00e42.m3u8',
    },
  ],
  WDSOLAR_VP_008: [],
  WDSOLAR_VP_009: [
    {
      source: 'http://hls01open.ys7.com/openlive/aafa9a15128845d5a4b365ec9f602dca.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/be02e3faeffa49f7b844f01809d96b77.m3u8',
    },
  ],
  WDSOLAR_VP_010: [
    {
      source: 'http://hls01open.ys7.com/openlive/97289333806a4d378361d7f404e17591.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/dca509189867420fb23267722ab9b62e.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/8020111549f34cbbaed5bd42737a416b.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/6131289aecf2436795f23c5bb3a00e42.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/6843bd3bb711499d87cc8f680fe2c9b8.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/fe2025635c8c4b3b842883f6a5a47531.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/bb9cf1d9710d4966b0c9ed432effa3f4.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/4f53bf600a414bc1b9d7dabe60bafda1.m3u8',
    },
  ],
  WDSOLAR_VP_011: [
    {
      source: 'http://hls01open.ys7.com/openlive/be02e3faeffa49f7b844f01809d96b77.m3u8',
    },
  ],
  WDSOLAR_VP_012: [
    {
      source: 'http://hls01open.ys7.com/openlive/ea115d49cc964485b3d61ed0f2d532ea.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/c7edcc758ef24fbea47bf6e4e1a6dbb1.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/f68a8885f2d2488ca89bd6398968ef7e.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/4a3a15f50c484e77b77292992e1876be.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/4e4366d12f2148e18c045be181ddcf97.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/636583fb48e84102a9068cdc4c06d1ba.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/5dcd3af9b13540cdb9f13b556f20e2b8.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/04ddef7237014fc1b742684e03215912.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/54190bd68f8746f6a72bfe7be9328be5.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/75f47b914f8b47d3a8b8d6352883e31e.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/e6003c2d180e48a79c5a40fa58dded55.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/428deed44a944ad6bce584c1ceae6b22.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/c8a8325baf8747e3aa808833d400e725.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/bea6f13395114be29bafe32ed75e2d94.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/d075f6c418ab4c0a95b942a6bdaa7e48.m3u8',
    },
    {
      source: 'http://hls01open.ys7.com/openlive/cb0d615e081d44bf9c885e41075fd13a.m3u8',
    },
  ],
};
export const STATIONS = {
  '/station/:id': (req: MockRequest) => {
    const pid = req.params.id;
    return stationsData[pid];
  },
  '/stationvideos/:id': (req: MockRequest) => {
    const pid = req.params.id;
    return stationVideos[pid];
  },
  '/station': JSON.parse(
    JSON.stringify({
      visitData,
      visitData2,
      salesData,
      searchData,
      offlineData,
      offlineChartData,
      salesTypeData,
      salesTypeDataOnline,
      salesTypeDataOffline,
      radarData,
      statisticData,
    }),
  ),
  '/station/visit': JSON.parse(JSON.stringify(visitData)),
  '/station/tags': Mock.mock({
    'list|100': [{ x: '@city', 'value|1-100': 150, 'category|0-2': 1 }],
  }),
};
