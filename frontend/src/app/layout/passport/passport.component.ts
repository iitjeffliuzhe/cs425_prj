import { Component } from '@angular/core';

@Component({
  selector: 'layout-passport',
  templateUrl: './passport.component.html',
  styleUrls: ['./passport.component.less'],
})
export class LayoutPassportComponent {
  links = [
    {
      title: '吴都能源',
      href: 'http://www.szwdny.com/',
    },
    {
      title: '隐私条款',
      href: './assets/tmp/privacy.txt',
    },
    {
      title: '帮助支持',
      href: 'http://www.tusiact.com/',
    },
  ];
}
