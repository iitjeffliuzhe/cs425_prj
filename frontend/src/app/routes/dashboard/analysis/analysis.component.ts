import { Component, ChangeDetectionStrategy, OnInit, ChangeDetectorRef } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd';
import { STColumn } from '@delon/abc';
import { getTimeDistance, deepCopy } from '@delon/util';
import { _HttpClient } from '@delon/theme';
import { I18NService } from '@core';
import { yuan } from '@shared';

@Component({
  selector: 'app-dashboard-analysis',
  templateUrl: './analysis.component.html',
  styleUrls: ['./analysis.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardAnalysisComponent implements OnInit {
  constructor(
    private http: _HttpClient,
    public msg: NzMessageService,
    private i18n: I18NService,
    private cdr: ChangeDetectorRef,
  ) {}
  data: any = {};
  loading = true;
  date_range: Date[] = [];
  // rankingListData: any[] = Array(7)
  //   .fill({})
  //   .map((item, i) => {
  //     return {
  //       title: this.i18n.fanyi('app.analysis.test', { no: i }),
  //       total: 323234,
  //     };
  //   });
  rankingListData: any[] = [
    {
      title: '雨润',
      total: 4025.6,
    },
    {
      title: '爱德',
      total: 2097.1,
    },
    {
      title: '锦富',
      total: 1755,
    },
    {
      title: '亿仕登',
      total: 1221.5,
    },
    {
      title: '联发',
      total: 1166.1,
    },
    {
      title: '奥英',
      total: 968.1,
    },
    {
      title: '康宁',
      total: 911.8,
    },
  ];
  titleMap = {
    y1: this.i18n.fanyi('预测发电量'),
    y2: this.i18n.fanyi('实际发电量'),
  };
  searchColumn: STColumn[] = [
    { title: '排名', i18n: 'app.analysis.table.rank', index: 'index' },
    {
      title: '搜索关键词',
      i18n: 'app.analysis.table.search-keyword',
      index: 'keyword',
      click: (item: any) => this.msg.success(item.keyword),
    },
    {
      type: 'number',
      title: '用户数',
      i18n: 'app.analysis.table.users',
      index: 'count',
      sorter: (a, b) => a.count - b.count,
    },
    {
      type: 'number',
      title: '周涨幅',
      i18n: 'app.analysis.table.weekly-range',
      index: 'range',
      render: 'range',
      sorter: (a, b) => a.range - b.range,
    },
  ];

  salesType = 'all';
  salesPieData: any;
  salesTotal = 0;

  saleTabs: any[] = [{ key: '发电量', show: true }, { key: '收入' }];
  salesData: any[] = [];
  incomeData: any[] = [];
  offlineIdx = 0;

  ngOnInit() {
    this.http.get('/chart').subscribe((res: any) => {
      res.offlineData.forEach((item: any, idx: number) => {
        item.show = idx === 0;
        item.chart = deepCopy(res.offlineChartData);
      });
      this.data = res;
      if (this.data.salesData) {
        for (let i = 0; i < this.data.salesData.length; i++) {
          this.salesData.push(this.data.salesData[i]);
          this.incomeData.push({ x: this.data.salesData[i].x, y: this.data.salesData[i].y * 0.62 });
          // console.log(this.salesData);
          // console.log(this.incomeData);
        }
      }
      this.loading = false;
      this.changeSaleType();
    });
  }

  setDate(type: any) {
    this.date_range = getTimeDistance(type);
    setTimeout(() => this.cdr.detectChanges());
  }
  changeSaleType() {
    this.salesPieData =
      this.salesType === 'all'
        ? this.data.salesTypeData
        : this.salesType === 'online'
        ? this.data.salesTypeDataOnline
        : this.data.salesTypeDataOffline;
    if (this.salesPieData) {
      this.salesTotal = this.salesPieData.reduce((pre, now) => now.y + pre, 0);
    }
    this.cdr.detectChanges();
  }

  handlePieValueFormat(value: any) {
    return yuan(value);
  }
  salesChange(idx: number) {
    if (this.saleTabs[idx].show !== true) {
      this.saleTabs[idx].show = true;
      this.cdr.detectChanges();
    }
  }
  offlineChange(idx: number) {
    if (this.data.offlineData[idx].show !== true) {
      this.data.offlineData[idx].show = true;
      this.cdr.detectChanges();
    }
  }
}
