import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd';
import { zip } from 'rxjs';
import { _HttpClient } from '@delon/theme';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { getTimeDistance, deepCopy } from '@delon/util';

@Component({
  selector: 'app-dashboard-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardVideosComponent implements OnInit, OnDestroy {
  constructor(
    private http: _HttpClient,
    private route: ActivatedRoute,
    public msg: NzMessageService,
    private cdr: ChangeDetectorRef,
    private sanitizer: DomSanitizer,
  ) {}
  id = this.route.snapshot.paramMap.get('id');
  data: any = {};
  tags = [];
  loading = true;
  q: any = {
    start: null,
    end: null,
  };
  percent: number | null = null;
  // region: active chart

  activeTime$: any;

  activeData: any[];

  player: any;
  station: any;

  titleMap = {
    y1: '预测发电量',
    y2: '实际发电量',
  };
  chartData: any[] = [];

  activeStat = {
    max: 0,
    min: 0,
    t1: '',
    t2: '',
  };

  // url = 'rtmp://rtmp01open.ys7.com/openlive/ea115d49cc964485b3d61ed0f2d532ea';
  // url =
  //   'https://open.ys7.com/ezopen/h5/iframe?url=ezopen://open.ys7.com/203751922/1.cloud.rec&autoplay=1&accessToken=ra.0ipedelv8nlw1r5ybvl5rddh7jnzdf1c-86rqdrbpjb-01ey10o-ua0cbzbyx';

  ngOnInit() {
    zip(this.http.get('/station/' + this.id), this.http.get('/station'), this.http.get('/station/tags')).subscribe(
      ([station, res, tags]: [any, any, any]) => {
        this.station = station;
        this.data = res;
        console.log(this.data);
        tags.list[Math.floor(Math.random() * tags.list.length) + 1].value = 1000;
        this.tags = tags.list;
        this.loading = false;
        this.cdr.detectChanges();
        this.chartData = deepCopy(res.offlineChartData);
      },
    );

    // active chart
    this.refData();
    this.activeTime$ = setInterval(() => this.refData(), 1000 * 2);
  }

  refData() {
    const activeData: any[] = [];
    for (let i = 0; i < 24; i += 1) {
      activeData.push({
        x: `${i.toString().padStart(2, '0')}:00`,
        y: i * 50 + Math.floor(Math.random() * 200),
      });
    }
    this.activeData = activeData;
    // stat
    this.activeStat.max = [...activeData].sort()[activeData.length - 1].y + 200;
    this.activeStat.min = [...activeData].sort()[Math.floor(activeData.length / 2)].y;
    this.activeStat.t1 = activeData[Math.floor(activeData.length / 2)].x;
    this.activeStat.t2 = activeData[activeData.length - 1].x;
    // percent
    this.percent = Math.floor(Math.random() * 100);
    this.cdr.detectChanges();
  }

  trustUrl(url: string) {
    if (url) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
  }
  // endregion

  couponFormat(val: any) {
    switch (parseInt(val, 10)) {
      case 20:
        return '差';
      case 40:
        return '中';
      case 60:
        return '良';
      case 80:
        return '优';
      default:
        return '';
    }
  }

  ngOnDestroy(): void {
    if (this.activeTime$) clearInterval(this.activeTime$);
  }
}
