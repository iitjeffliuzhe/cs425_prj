import { Component, OnInit, ViewChild, TemplateRef, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NzMessageService, NzModalService } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { tap, map } from 'rxjs/operators';
import { STComponent, STColumn, STData, STChange } from '@delon/abc';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProTableListComponent implements OnInit {
  id = this.route.snapshot.paramMap.get('id');

  q: any = {
    pi: 1,
    ps: 10,
    sorter: '',
    status: null,
    statusList: [],
  };
  data: any[] = [];
  loading = false;
  status = [
    { index: 0, text: '关闭', value: false, type: 'default', checked: false },
    {
      index: 1,
      text: '运行中',
      value: false,
      type: 'processing',
      checked: false,
    },
    { index: 2, text: '已上线', value: false, type: 'success', checked: false },
    { index: 3, text: '运行中', value: false, type: 'process', checked: false },
    // { index: 3, text: '异常', value: false, type: 'error', checked: false },
  ];
  @ViewChild('st', { static: true })
  st: STComponent;
  columns: STColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '设备编号', index: 'no' },
    { title: '描述', index: 'description' },
    {
      title: '设备在线时长',
      index: 'callNo',
      type: 'number',
      format: (item: any) => `${item.callNo} 小时`,
      sorter: (a: any, b: any) => a.callNo - b.callNo,
    },
    {
      title: '设备状态',
      index: 'status',
      render: 'status',
      filter: {
        menus: this.status,
        fn: (filter: any, record: any) => record.status === filter.index,
      },
    },
    // {
    //   title: '更新时间',
    //   index: 'updatedAt',
    //   type: 'date',
    //   sort: {
    //     compare: (a: any, b: any) => a.updatedAt - b.updatedAt,
    //   },
    // },
    {
      title: '操作',
      buttons: [
        {
          text: '状态测试',
          click: (item: any) => this.msg.success(`设备${item.no}状态测试正常`),
        },
        {
          text: '订阅警报',
          click: (item: any) => this.msg.success(`订阅警报成功${item.no}`),
        },
      ],
    },
  ];
  selectedRows: STData[] = [];
  description = '';
  totalCallNo = 0;
  expandForm = false;

  constructor(
    private http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: NzModalService,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.loading = true;
    this.q.statusList = this.status.filter(w => w.checked).map(item => item.index);
    if (this.q.status !== null && this.q.status > -1) {
      this.q.statusList.push(this.q.status);
    }
    this.http
      .get('/rule', this.q)
      .pipe(
        map((list: any[]) =>
          list.map(i => {
            const statusItem = this.status[i.status];
            i.statusText = statusItem.text;
            i.statusType = statusItem.type;
            return i;
          }),
        ),
        tap(() => (this.loading = false)),
      )
      .subscribe(res => {
        this.data = res;
        if (this.data.length > 5) {
          this.data[0].no = 'VP' + this.id.substring(this.id.length - 3) + '_101';
          this.data[0].description = '并网表';
          this.data[0].callNo = 8642;
          this.data[0].status = 2;
          this.data[1].no = 'VP' + this.id.substring(this.id.length - 3) + '_102';
          this.data[1].description = '上网表';
          this.data[1].callNo = 8636;
          this.data[1].status = 2;
          this.data[2].no = 'VP' + this.id.substring(this.id.length - 3) + '_103';
          this.data[2].description = '上网表';
          this.data[2].callNo = 8529;
          this.data[2].status = 2;
          this.data[3].no = 'VP' + this.id.substring(this.id.length - 3) + '_104';
          this.data[3].description = '上网表';
          this.data[3].callNo = 8531;
          this.data[3].status = 2;
        }
        for (let i = 4; i < this.data.length; i++) {
          let str = i.toString(10);
          if (i - 3 < 10) {
            str = '0' + str;
          }
          this.data[i].no = 'VP' + this.id.substring(this.id.length - 3) + '_3' + str;
          this.data[i].description = '逆变器';
        }
        this.cdr.detectChanges();
      });

    // console.log(this.data);
  }

  stChange(e: STChange) {
    switch (e.type) {
      case 'checkbox':
        this.selectedRows = e.checkbox!;
        this.totalCallNo = this.selectedRows.reduce((total, cv) => total + cv.callNo, 0);
        this.cdr.detectChanges();
        break;
      case 'filter':
        this.getData();
        break;
    }
  }

  remove() {
    this.http.delete('/rule', { nos: this.selectedRows.map(i => i.no).join(',') }).subscribe(() => {
      this.getData();
      this.st.clearCheck();
    });
  }

  approval() {
    this.msg.success(`审批了 ${this.selectedRows.length} 笔`);
  }

  add(tpl: TemplateRef<{}>) {
    this.modalSrv.create({
      nzTitle: '新建规则',
      nzContent: tpl,
      nzOnOk: () => {
        this.loading = true;
        this.http.post('/rule', { description: this.description }).subscribe(() => this.getData());
      },
    });
  }

  reset() {
    // wait form reset updated finished
    setTimeout(() => this.getData());
  }
}
